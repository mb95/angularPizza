var app = angular.module('myApp', ['ui.router']);

    app.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {

        $urlRouterProvider.otherwise('/pizza');

        $stateProvider
            .state('home', {
                url: '/pizza',
                templateUrl: 'templates/pizza.html'
            })
            .state('warenkorb', {
                url: '/warenkorb',
                templateUrl: 'templates/warenkorb.html'
            })
            .state('info', {
                url: '/info',
                templateUrl: 'templates/info.html'
            });

    }]);

    app.controller('pizzaController', ['$scope', '$http', function ($scope, $http) {

        $scope.list = [];

        $http.get('data/pizzaList.json').then(function (response) {
            $scope.list = response.data;
        }, function (response) {
            alert("Error getting Pizzalist: " + response.content);
        });

        $scope.warenkorb = [];
        $scope.gesamtKosten = 0;

        $scope.addToWarenkorb = function (item) {
            $scope.warenkorb.push(item);
            $scope.gesamtKosten += item.kosten;
        };

        $scope.emptyWarenkorb = function () {
            $scope.warenkorb = [];
            $scope.gesamtKosten = 0;
        };

        $scope.makeSendObject = function () {

            var time = new Date().toTimeString();

            var date = new Date().toDateString();

            return '{ "order":' + JSON.stringify($scope.warenkorb) + ', "time": ' + JSON.stringify(time) + ', "date": ' + JSON.stringify(date) + '   }';
        };

        $scope.placeOrder = function () {

            var sendObj = $scope.makeSendObject();

            $http({
                url: '/',
                method: 'POST',
                data: sendObj,
                headers: {'Content-Type': 'application/json'}
            }).then(function (res) {
                console.log("Server says: " + res.data)
            })
        }
    }]);

